
docker build . --platform=linux/amd64 --tag runtime-amd64

crossplane xpkg build \
    --package-root=package \
    --embed-runtime-image=runtime-amd64 \
    --package-file=function-amd64.xpkg

crossplane xpkg push \
  --package-files=function-amd64.xpkg \
  registry.gitlab.com/corewire/images/crossplane/function-keycloak-builtin-objects:dev

kubectl delete -f example/
kubectl apply -f example/01-xrd.yaml
kubectl apply -f example/02-composition.yaml
kubectl apply -f example/03-functions.yaml

# wait for the function to be ready
until kubectl get function function-keycloak-builtin-objects -o jsonpath='{.status.conditions[?(@.type=="Healthy")].status}' | grep True; do sleep 1; done

kubectl apply -f example/04-xr-master.yaml