"""A Crossplane composition function."""

import grpc
from crossplane.function import logging, response
from crossplane.function.proto.v1beta1 import run_function_pb2 as fnv1beta1
from crossplane.function.proto.v1beta1 import run_function_pb2_grpc as grpcv1beta1
from keycloak import KeycloakAdmin
from google.protobuf import json_format
import json
import base64

REALM_ADMINS = dict()

BUILTIN_CLIENTS = [
    "account",
    "account-console",
    "admin-cli",
    "broker",
    "master-realm",
    "security-admin-console",
]

BUILTIN_ROLES = [
    "admin",
    "create-client",
    "create-realm",
    "delete-account",
    "impersonation",
    "manage-account-links",
    "manage-account",
    "manage-authorization",
    "manage-clients",
    "manage-consent",
    "manage-events",
    "manage-identity-providers",
    "manage-realm",
    "manage-users",
    "offline_access",
    "query-clients",
    "query-groups",
    "query-realms",
    "query-users",
    "read-token",
    "realm-admin",
    "uma_authorization",
    "view-applications",
    "view-authorization",
    "view-clients",
    "view-consent",
    "view-events",
    "view-groups",
    "view-identity-providers",
    "view-profile",
    "view-realm",
    "view-users",
]


class FunctionRunner(grpcv1beta1.FunctionRunnerService):
    """A FunctionRunner handles gRPC RunFunctionRequests."""

    def __init__(self):
        """Create a new FunctionRunner."""
        self.log = logging.get_logger()


    def create_client_object(self, realm, client_uuid, client_name, keycloak_config_name):
        self.log.info(f"Creating client object for {client_name} in realm {realm} with uuid {client_uuid} and keycloak config {keycloak_config_name}")
        client_name = client_name.replace("_", "-")
        client = {
            "apiVersion": "openidclient.keycloak.crossplane.io/v1alpha1",
            "kind": "Client",
            "metadata": {
                "annotations": {"crossplane.io/external-name": f"{realm}/{client_uuid}"},
                "name": f"builtin-{realm}-{client_name}",
            },
            "spec": {
                "forProvider": {},
                "providerConfigRef": {"name": keycloak_config_name},
                "managementPolicies": ["Observe"],
            },
        }
        return client


    def create_role_object(self, realm, role_name, role_uuid, keycloak_config_name):
        self.log.info(f"Creating role object for {role_name} in realm {realm} with uuid {role_uuid} and keycloak config {keycloak_config_name}")
        role_name = role_name.replace("_", "-")
        role = {
            "apiVersion": "role.keycloak.crossplane.io/v1alpha1",
            "kind": "Role",
            "metadata": {
                "name": f"builtin-{realm}-{role_name}",
                "annotations": {"crossplane.io/external-name": f"{realm}/{role_uuid}"},
            },
            "spec": {
                "forProvider": {},
                "providerConfigRef": {"name": keycloak_config_name},
                "managementPolicies": ["Observe"],
            },
        }
        return role

    def create_default_role_object(self, realm, role_name, role_uuid, keycloak_config_name, roles):
        self.log.info(f"Creating role object for {role_name} in realm {realm} with uuid {role_uuid} and keycloak config {keycloak_config_name}")
        role_name = role_name.replace("_", "-")
        role = {
            "apiVersion": "role.keycloak.crossplane.io/v1alpha1",
            "kind": "Role",
            "metadata": {
                "name": f"builtin-{realm}-{role_name}",
                "annotations": {"crossplane.io/external-name": f"{realm}/{role_uuid}"},
            },
            "spec": {
                "forProvider": {
                    "realmId": realm,
                    "name": role_name,
                    "compositeRoles": [roles],

                },
                "providerConfigRef": {"name": keycloak_config_name},
                "managementPolicies": ["*"],
            },
        }
        return role


    def process_realm(self, realm_name, provider_config_name, response, request):
        clients_list = request.observed.composite.resource["spec"]["builtinClients"]
        realm_roles_list = request.observed.composite.resource["spec"]["builtinRealmRoles"]

        self.log.info(f"Processing Realm {realm_name}")
        self.log.info(f"* Realm: {realm_name}")
        self.log.info(f"* Provider Config: {provider_config_name}")
        self.log.info(f"* Builtin Clients: {clients_list}")
        self.log.info(f"* Builtin Realm Roles: {realm_roles_list}")

        realm_admin = REALM_ADMINS[realm_name]
        clients = realm_admin.get_clients()
        for client in clients:
            client_name = client.get("clientId")
            client_uuid = client.get("id")
            if client_name not in BUILTIN_CLIENTS and client_name not in clients_list:
                continue
            client_object = self.create_client_object(
                realm_name, client_uuid, client_name, provider_config_name
            )
            response.desired.resources[
                f"builtin-{realm_name}-{client_name}"
            ].resource.update(client_object)
            client_roles = realm_admin.get_client_roles(client_uuid)
            for role in client_roles:
                if role.get("name") not in BUILTIN_ROLES:
                    continue
                role_name = f'{client_name}-{role.get("name")}'
                role_uuid = role.get("id")
                role_object = self.create_role_object(
                    realm_name, role_name, role_uuid, provider_config_name
                )
                response.desired.resources[
                    f"builtin-{realm_name}-{role_name}"
                ].resource.update(role_object)

        realm_roles = realm_admin.get_realm_roles()
        for role in realm_roles:
            if role.get("name") not in BUILTIN_ROLES or role.get("name") not in realm_roles_list:
                continue
            role_name = f'realm-role-{role.get("name")}'
            role_uuid = role.get("id")
            role_object = self.create_role_object(
                realm_name, role_name, role_uuid, provider_config_name
            )
            response.desired.resources[f"builtin-{realm_name}-{role_name}"].resource.update(
                role_object
            )


    def get_keycloak_provider_credentials(self, provider_credentials_secret_name, raw_secrets):
        provider_credentials_dict = json_format.MessageToDict(raw_secrets)
        for secret in provider_credentials_dict:
            if secret["metadata"]["name"] == provider_credentials_secret_name:
                data_encoded = secret["data"]["credentials"]
                data_decoded = base64.b64decode(data_encoded).decode("utf-8")
                data_dict = json.loads(data_decoded)
                return data_dict


    async def RunFunction(
        self, req: fnv1beta1.RunFunctionRequest, _: grpc.aio.ServicerContext
    ) -> fnv1beta1.RunFunctionResponse:
        """Run the function."""
        log = self.log.bind(tag=req.meta.tag)
        log.info("Running function")

        # XR properties
        provider_config_name = req.observed.composite.resource["spec"][
            "providerConfigName"
        ]
        provider_credentials_secret_name = req.observed.composite.resource["spec"][
            "providerSecretName"
        ]

        realm_name = req.observed.composite.resource["spec"]["realm"]

        # Get the provider credentials dicts
        extra_resources = req.context["apiextensions.crossplane.io/extra-resources"]
        keycloak_credentials_dict = self.get_keycloak_provider_credentials(
            provider_credentials_secret_name, extra_resources["secrets"]
        )
        if not keycloak_credentials_dict:
            log.error(
                f"Could not find matching secret for providerSecretName: {provider_credentials_secret_name}"
            )
            return response.to(req)

        username = keycloak_credentials_dict.get("username", None)
        password = keycloak_credentials_dict.get("password", None)
        client_id = keycloak_credentials_dict.get("client_id", None)
        client_secret = keycloak_credentials_dict.get("client_secret", None)
        keycloak_url = keycloak_credentials_dict["url"]
        base_path = keycloak_credentials_dict.get("base_path", None)

        if client_id in [None, "", "null"] or client_secret in [None, "", "null"]:
            if username in [None, "", "null]"] or password in [None, "", "null"]:
                log.error("Could not extract credentials from the passed secret")
                return response.to(req)
            
            credentials = {"username": username, "password": password}
        else:
            credentials = {"client_id": client_id, "client_secret_key": client_secret}           
        
        if base_path:
            keycloak_url = f"{keycloak_url}/{base_path}/"

        rsp = response.to(req)

        ## validate variables
        assert realm_name, "realm_name is required"
        assert keycloak_url, "keycloak_url is required"
        assert credentials, "credentials is required"
        assert provider_config_name, "provider_config_name is required"

        REALM_ADMINS[realm_name] = KeycloakAdmin(
            **credentials,
            server_url=keycloak_url,
            realm_name=realm_name,
            user_realm_name="master",
        )
        log.info(f"Keycloak Admin for Realm {realm_name} created")
        self.process_realm(realm_name, provider_config_name, rsp, req)
        return rsp
